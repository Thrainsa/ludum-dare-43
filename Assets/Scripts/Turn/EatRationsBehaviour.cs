﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;
using ZBGE.UI;

public class EatRationsBehaviour : StateMachineBehaviour
{
  private UIManager uiManager;

  private GroupManager groupManager;

  // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    uiManager = FindObjectOfType<UIManager>();
    if (uiManager == null) Debug.LogError("Scene has no UI Manager");

    groupManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
    if (groupManager == null) Debug.LogError("Scene has no Group Manager");

    animator.SetTrigger("rationsEaten");
  }

  // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
  //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    
  //}

  // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    CharacterGroup group = groupManager.group;
    string result = "It's time to eat.\r\n";
    if(group.rations < group.Characters.Count) {
      result += "We don't have enought food. "+ (group.Characters.Count - group.rations) + " people can't eat and loose sanity.";
    }
    uiManager.ShowInfoPopup(result);
    foreach (var character in group.GetCharacters(CharacterSort.POWER))
    {
      if (group.rations > 0)
      {
        group.rations--;
        character.AddStamina(Character.MAX_STAMINA);
      }
      else
      {
        character.AddStamina(-1);
        character.AddSanity(-1);
      }
    }

    animator.SetInteger("timeOfDay", 6);
  }

  // OnStateMove is called right after Animator.OnAnimatorMove()
  //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    // Implement code that processes and affects root motion
  //}

  // OnStateIK is called right after Animator.OnAnimatorIK()
  //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    // Implement code that sets up animation IK (inverse kinematics)
  //}
}
