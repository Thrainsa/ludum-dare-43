﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;
using ZBGE.map;
using ZBGE.UI;
using ZBGE.data;

public class RandomGroupEventBehaviour : StateMachineBehaviour
{
  public Database database;

  private UIManager uiManager;

  private GroupManager groupManager;

  private PlayerManager playerManager;

  private RandomEventUI randomEventUI;
  private ConfirmUI confirmUI;

  private Animator animator;

  private ZBGE.data.Event currentEvent;

  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    this.animator = animator;
    this.currentEvent = null;

    uiManager = FindObjectOfType<UIManager>();
    if (uiManager == null) Debug.LogError("Scene has no UI Manager");

    groupManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
    if (groupManager == null) Debug.LogError("Scene has no Group Manager");

    // Pick an event
    TileComponent currentTile = groupManager.CurrentTile;
    if (currentTile.randomEvent)
    {
      // Existing event
      currentEvent = currentTile.randomEvent;
    }
    else if (!currentTile.eventDrawn)
    {
      // Pick random one
      currentTile.eventDrawn = true;
      EventContext eventContext = new EventContext(EventTriggerHook.MOVEMENT, currentTile.terrainType);
      List<ZBGE.data.Event> possibleEvents = database.GetMatchingEvents(eventContext);
      if (possibleEvents.Count > 0) currentEvent = possibleEvents[Random.Range(0, possibleEvents.Count)];
      else Debug.Log("No matching event found");
    }

    // Run the event or skip
    if (currentEvent != null)
    {
      Debug.Log("Running event: " + currentEvent.name);
      uiManager.ConfigureRandomEventUI(currentEvent.title, currentEvent.text, currentEvent.choices);
      randomEventUI = uiManager.SetRandomEventUIVisible(true);
      randomEventUI.OnChoiceClickEvent += OnEventChoice;
      
      PlayerManager playerManager = FindObjectOfType<PlayerManager>();
      playerManager.PlayClip(playerManager.alertSound);
      return;
    }
    else
    {
      RandomEventOver();
    }
  }

  public void OnEventChoice(int index)
  {
    randomEventUI.OnChoiceClickEvent -= OnEventChoice;
    uiManager.SetRandomEventUIVisible(false);

    EventChoice choice = currentEvent.choices[index];
    bool success = true;
    if (choice.test)
    {
      success = choice.test.Resolve(groupManager);
    }
    else
    {
      Debug.LogWarning("Event " + currentEvent.name + " has a choice with no test");
    }

    choice.ApplyOutcomes(currentEvent, playerManager, groupManager, success);

    string text;
    if (success)
      text = choice.outcomeSuccessText;
    else
      text = choice.outcomeFailureText;

    uiManager.ConfigureConfirmUI("Event outcome", text);
    confirmUI = uiManager.SetConfirmUIVisible(true);
    confirmUI.OnCloseEvent += OnConfirmClose;
  }

  public void OnConfirmClose()
  {
    confirmUI.OnCloseEvent -= OnConfirmClose;
    RandomEventOver();
  }

  private void RandomEventOver()
  {
    uiManager.SetConfirmUIVisible(false);
    animator.SetTrigger("randomEventOver");
  }

  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
  }

}
