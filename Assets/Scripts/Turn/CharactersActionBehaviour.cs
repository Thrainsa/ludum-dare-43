﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;
using ZBGE.map;
using ZBGE.UI;
using ZBGE.data;

public class CharactersActionBehaviour : StateMachineBehaviour
{

  public Database database;

  public ActionList validateActionList;

  public int actionPerDay = 1;

  private UIManager uiManager;

  private GroupManager groupManager;

  private PlayerManager playerManager;

  private Animator animator;

  private ActionUI validateUI;

  private Action selectedAction = null;

  private TileComponent selectedTile = null;

  private int actionsStarted = 0;

  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    this.animator = animator;
    this.selectedAction = null;
    this.selectedTile = null;
    actionsStarted = 0;

    uiManager = FindObjectOfType<UIManager>();
    if (uiManager == null) Debug.LogError("Scene has no UI Manager");

    groupManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
    if (groupManager == null) Debug.LogError("Scene has no Group Manager");

    playerManager = FindObjectOfType<PlayerManager>();
    if (playerManager == null) Debug.LogError("Scene has no Player Manager");

    uiManager.SetCharactersActionPanelVisible(true);
    uiManager.OnActionSelected += OnActionSelected;
  }

  public void OnActionSelected(Action action)
  {
    if (!isValidationAction(action))
    {
      // Choose action
      uiManager.SetCharactersActionPanelVisible(false);
      if (Action.ACTION_END_OF_TURN.Equals(action.title))
      {
        animator.SetInteger("actionsRemaining", 0);
        animator.SetTrigger("endOfTurn");
      }
      else
      {
        selectedAction = action;
        uiManager.groupManagerUI.CanSelectCharacters = true;
        validateUI = uiManager.SetValidatePanelVisible(true);

        if (Action.ACTION_EXPLORE.Equals(selectedAction.title))
        {
          playerManager.CanSelectTile = true;
          playerManager.OnSelectTileEvent += SelectTile;
        }
      }
    }
    else
    {
      // Confirm/Cancel
      if (Action.ACTION_CANCEL.Equals(action.title))
      {
        uiManager.SetCharactersActionPanelVisible(true);
      }
      else if (Action.ACTION_CONFIRM.Equals(action.title))
      {
        if (Action.ACTION_EXPLORE.Equals(selectedAction.title) && selectedTile == null)
        {
          uiManager.ShowInfoPopup("You need to select a tile to explore");
          return;
        }
        else if (uiManager.groupManagerUI.SelectedCharacters.Count == 0)
        {
          uiManager.ShowInfoPopup("You need to select at least one character for the action");
          return;
        }
        else if (uiManager.groupManagerUI.SelectedCharacters.Count > 5)
        {
          uiManager.ShowInfoPopup("You need to select less than 5 characters for the action");
          return;
        }
        selectedAction.Execute(uiManager, groupManager, uiManager.groupManagerUI.SelectedCharacters, selectedTile);
        uiManager.groupManagerUI.UnselectAll();
        animator.SetInteger("actionsRemaining", animator.GetInteger("actionsRemaining") + 1);
        actionsStarted++;

        if (actionPerDay >= actionsStarted)
        {
          if(Action.ACTION_EXPLORE.Equals(selectedAction.title)) {
            playerManager.CanSelectTile = false;
            playerManager.OnSelectTileEvent -= SelectTile;
            animator.SetTrigger("choseExplore");
          } else {
            // For now we resolve the actions here
            animator.SetInteger("actionsRemaining", 0);
            animator.SetTrigger("endOfTurn");
          }
        }
        else
        {
          animator.SetTrigger("choseAction");
        }
      }
      uiManager.groupManagerUI.CanSelectCharacters = false;
      uiManager.SetValidatePanelVisible(false);
    }
  }

  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    uiManager.OnActionSelected -= OnActionSelected;
  }

  private void SelectTile(TileComponent tile)
  {
    this.selectedTile = tile;
  }

  private bool isValidationAction(Action action)
  {
    foreach (Action validateAction in validateActionList.actions)
    {
      if (validateAction.Equals(action)) return true;
    }
    return false; ;
  }
}
