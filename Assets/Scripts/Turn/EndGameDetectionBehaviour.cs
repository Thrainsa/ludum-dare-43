﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;

public class EndGameDetectionBehaviour : StateMachineBehaviour
{
  private Objective objective;

  private GroupManager groupManager;

  private Animator animator;

  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    this.animator = animator;

    objective = FindObjectOfType<Objective>();
    if (objective == null) Debug.LogError("Scene has no objective");
    objective.OnGroupCollisionEvent += OnObjectiveCollision;
    
    groupManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
    if (groupManager == null) Debug.LogError("Scene has no Group Manager");
    groupManager.group.OnGroupChange += OnGroupChange;
  }

  public void OnObjectiveCollision(GroupMovement group)
  {
    TitleScreen.victory = true;
    animator.SetTrigger("victory");
  }

  public void OnGroupChange(List<Character> characters)
  {
    if (characters.Count == 0)
    {
      TitleScreen.gameover = true;
      animator.SetTrigger("defeat");
    }
  }

  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    objective.OnGroupCollisionEvent -= OnObjectiveCollision;
    groupManager.group.OnGroupChange -= OnGroupChange;
  }

}
