﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;
using ZBGE.map;
using ZBGE.UI;
using ZBGE.data;

public class RandomExploreEventBehaviour : StateMachineBehaviour
{
  public Database database;

  private UIManager uiManager;

  private GroupManager exploreGroupManager;

  private PlayerManager playerManager;

  private RandomEventUI randomEventUI;
  private ConfirmUI confirmUI;

  private Animator animator;

  private ZBGE.data.Event currentEvent;
  private List<Character> initialExploreCharacters;


  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    this.animator = animator;
    this.currentEvent = null;

    uiManager = FindObjectOfType<UIManager>();
    if (uiManager == null) Debug.LogError("Scene has no UI Manager");

    exploreGroupManager = GameObject.FindGameObjectWithTag("ExploreGroup").GetComponent<GroupManager>();
    if (exploreGroupManager == null) Debug.LogError("Scene has no ExploreGroup Group Manager");

    initialExploreCharacters = exploreGroupManager.group.Characters;

    // Pick an event
    TileComponent currentTile = exploreGroupManager.CurrentTile;
    if (currentTile.randomEvent)
    {
      // Existing event
      currentEvent = currentTile.randomEvent;
    }
    else if (!currentTile.eventDrawn)
    {
      // Pick random one
      currentTile.eventDrawn = true;
      EventContext eventContext = new EventContext(EventTriggerHook.MOVEMENT, currentTile.terrainType);
      List<ZBGE.data.Event> possibleEvents = database.GetMatchingEvents(eventContext);
      if (possibleEvents.Count > 0) currentEvent = possibleEvents[Random.Range(0, possibleEvents.Count)];
      else Debug.Log("No matching event found");
    }

    // Run the event or skip
    if (currentEvent != null)
    {
      ScoutEvent();
      return;
    }
    else
    {
      uiManager.ShowInfoPopup("You find a peaceful place. Nothing important to report.");
      RandomEventOver();
    }
  }

  private void ScoutEvent()
  {
    playerManager = FindObjectOfType<PlayerManager>();
    playerManager.PlayClip(playerManager.alertSound);

    Debug.Log("Running Scouting event: " + currentEvent.name);
    bool success = currentEvent.scoutingTest.Resolve(exploreGroupManager);
    EventOutcome[] outcomes = success ? currentEvent.scoutingSuccessOutcomes : currentEvent.scoutingFailureOutcomes;
    EventOutcome.ApplyOutcomes(currentEvent, playerManager, exploreGroupManager, outcomes, true);

    string text;
    if (success)
      text = currentEvent.scoutingSuccessText;
    else
      text = currentEvent.scoutingFailureText;

    uiManager.ConfigureConfirmUI("Event outcome", text);
    confirmUI = uiManager.SetConfirmUIVisible(true);
    confirmUI.OnCloseEvent += OnConfirmClose;
    
  }

  public void OnConfirmClose()
  {
    confirmUI.OnCloseEvent -= OnConfirmClose;
    RandomEventOver();
  }

  private void RandomEventOver()
  {
    uiManager.SetConfirmUIVisible(false);
    animator.SetTrigger("endExplore");
  }

  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    // We regroup the Explore Group with the main group
    GroupManager mainGroup = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
    Debug.Log("end of explore");
    List<Character> newCharacters = new List<Character>();
    foreach (var item in exploreGroupManager.group.Characters)
    {
      bool removed = initialExploreCharacters.Remove(item);
      if(!removed) {
        newCharacters.Add(item);
      } 
    }

    initialExploreCharacters.ForEach(c => mainGroup.group.LostCharacter(c));
    newCharacters.ForEach(c => mainGroup.group.GainCharacter(c));

    exploreGroupManager.movement.SetCurrentTile(mainGroup.movement.CurrentTile);
  }

}
