﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarUI : MonoBehaviour
{
  public Image progressBarImg;
  public Color goodColor;
  public Color averageColor;
  public Color badColor;

  public float maxValue;
  public float currentValue;

  public void SetValue(float newValue)
  {
    float ratio = newValue / maxValue;
    ratio = Mathf.Clamp(ratio, 0.05f, 1f);
    progressBarImg.rectTransform.localScale = new Vector3(ratio, 1, 1);
    if (ratio <= 0.25f)
    {
      progressBarImg.color = badColor;
    }
    else if (ratio <= 0.5f)
    {
      progressBarImg.color = averageColor;
    }
    else
    {
      progressBarImg.color = goodColor;
    }
  }

}
