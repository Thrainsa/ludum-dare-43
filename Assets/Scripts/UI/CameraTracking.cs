﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;

namespace ZBGE.UI
{
  public class CameraTracking : MonoBehaviour
  {

    private static float SMOOTH_TIME = 0.3f;

    private static float MAX_SPEED = 5.0f;

    private static float MENU_HEIGHT_UNITS = 5.0f;

    private static Vector3 INITIAL_CAMERA_OFFSET = Vector3.down * 5;

    private static Vector3 CAMERA_OFFSET = Vector3.back * 5;

    public Transform target;

    private Vector2 currentVelocity;

    void Start()
    {
      GameObject playerObject = GameObject.FindGameObjectWithTag("Player");
      if (playerObject != null)
      {
        GroupManager group = playerObject.GetComponent<GroupManager>();
        target = group.transform;
        transform.position = target.position + INITIAL_CAMERA_OFFSET;
      }
    }

    void Update()
    {
      if (target != null)
      {
        transform.position = Vector2.SmoothDamp(transform.position, target.position + Vector3.down * MENU_HEIGHT_UNITS / 2, ref currentVelocity, SMOOTH_TIME, MAX_SPEED);
        transform.position += CAMERA_OFFSET;
      }
    }
  }

}