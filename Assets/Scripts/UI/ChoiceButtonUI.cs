﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace ZBGE.UI
{
  public class ChoiceButtonUI : MonoBehaviour
  {

    public RandomEventUI randomEventUI;
    public int buttonIndex;

    public TextMeshProUGUI text;
    public Image explorationIcon;
    public Image combatIcon;

    public void Init(RandomEventUI ui, int buttonIndex, string text, bool useCombat, bool useExploration)
    {
      this.randomEventUI = ui;
      this.buttonIndex = buttonIndex;
      this.text.text = text;
      // if(useCombat) combatIcon.gameObject.SetActive(true);
      // if(useExploration) explorationIcon.gameObject.SetActive(true);
    }

    public void OnClick()
    {
      randomEventUI.OnChoiceButtonClick(buttonIndex);
    }
  }

}