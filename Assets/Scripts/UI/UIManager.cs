﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZBGE.data;
using ZBGE.Game;

namespace ZBGE.UI
{
  public class UIManager : MonoBehaviour
  {
    public delegate void ActionSelect(Action action);
    public event ActionSelect OnActionSelected;

    public GroupManagerUI groupManagerUI;
    public ActionUI groupActionUI;
    public ActionUI charactersActionUI;
    public ActionUI validateUI;
    public RandomEventUI randomEventUI;

    public ConfirmUI confirmUI;
    public PopupInfoUI popupInfo;

    public CanvasGroup newDayPanel;
    public TextMeshProUGUI newDayPanelTxt;

    public CanvasGroup startPanel;

    void Start()
    {
      groupActionUI.OnActionSelected += SelectAction;
      charactersActionUI.OnActionSelected += SelectAction;
      validateUI.OnActionSelected += SelectAction;
    }

    public ActionUI SetGroupActionPanelVisible(bool visible)
    {
      groupActionUI.SetVisible(visible);
      return groupActionUI;
    }

    public ActionUI SetCharactersActionPanelVisible(bool visible)
    {
      charactersActionUI.SetVisible(visible);
      return charactersActionUI;
    }

    public ActionUI SetValidatePanelVisible(bool visible)
    {
      validateUI.SetVisible(visible);
      return validateUI;
    }

    private void SelectAction(Action action)
    {
      PlayerManager playerManager = FindObjectOfType<PlayerManager>();
      playerManager.PlayClip(playerManager.click2Sound);
      Debug.Log("Event ActionSelected: " + action.title);
      if (OnActionSelected != null)
      {
        OnActionSelected(action);
      }
    }

    public RandomEventUI SetRandomEventUIVisible(bool visible)
    {
      randomEventUI.gameObject.SetActive(visible);
      return randomEventUI;
    }

    public void ConfigureRandomEventUI(string title, string text, EventChoice[] choices)
    {
      randomEventUI.SetTitle(title);
      randomEventUI.SetText(text);
      randomEventUI.SetChoices(choices);
    }

    public void SetStartPanelVisible(bool visible)
    {
      startPanel.gameObject.SetActive(visible);
    }

    public ConfirmUI SetConfirmUIVisible(bool visible)
    {
      confirmUI.gameObject.SetActive(visible);
      return confirmUI;
    }

    public void ConfigureConfirmUI(string title, string text)
    {
      confirmUI.SetTitle(title);
      confirmUI.SetText(text);
    }

    public void ShowInfoPopup(string text)
    {
      popupInfo.Show(text);
    }

    public void NextDay(int day)
    {
      if(day != 1)
        newDayPanel.alpha = 0;
      else
        newDayPanel.alpha = 1;
      newDayPanel.gameObject.SetActive(true);
      newDayPanelTxt.SetText("DAY "+ day);
      StartCoroutine(NextDayFadeInOut(5f));
    }

    private IEnumerator NextDayFadeInOut(float duration)
    {
      while(newDayPanel.alpha < 1){
        newDayPanel.alpha += Time.fixedDeltaTime / 2f;
        yield return Time.fixedDeltaTime;
      }
      yield return new WaitForSeconds(1);

      while(newDayPanel.alpha > 0){
        newDayPanel.alpha -= Time.fixedDeltaTime / 2f;
        yield return Time.fixedDeltaTime;
      }
      newDayPanel.gameObject.SetActive(false);
    }


    private void FadeOut()
    {
      newDayPanel.alpha -= Time.fixedDeltaTime / 2f;
    }
  }
}