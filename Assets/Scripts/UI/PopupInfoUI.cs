﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ZBGE.UI
{
  public class PopupInfoUI : MonoBehaviour
  {
    public TextMeshProUGUI popupText;

    private Image image;
    private Coroutine showCoroutine = null;

    void Awake()
    {
      image = GetComponent<Image>();
    }

    public void Show(string text, float duration = 5f)
    {
      gameObject.SetActive(true);
      if(showCoroutine != null) {
        StopCoroutine (showCoroutine);
      }
      popupText.SetText(text);
      showCoroutine = StartCoroutine(DisplayPopup(duration));
    }

    private IEnumerator DisplayPopup(float duration)
    {
      image.GetComponent<CanvasRenderer>().SetAlpha(0f);
      popupText.GetComponent<CanvasRenderer>().SetAlpha(0f);
      FadeIn();
      yield return new WaitForSeconds(duration);

      FadeOut();
      yield return new WaitForSeconds(2);
      gameObject.SetActive(false);
    }

    private void FadeIn()
    {
      image.CrossFadeAlpha(1f, 1f, true);
      popupText.CrossFadeAlpha(1f, 1f, true);
    }

    private void FadeOut()
    {
      image.CrossFadeAlpha(0f, 1f, true);
      popupText.CrossFadeAlpha(0f, 1f, true);
    }
  }
}