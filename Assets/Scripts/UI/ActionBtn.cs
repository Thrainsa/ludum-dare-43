﻿using TMPro;
using UnityEngine;
using ZBGE.data;

namespace ZBGE.UI
{
  public class ActionBtn : MonoBehaviour
  {
    public delegate void ActionSelect(Action action);
    public event ActionSelect OnActionSelected;

    public TextMeshProUGUI text;

    [HideInInspector]
    public Action action;

    public void Init(Action action)
    {
      this.action = action;
      this.text.SetText(action.title);
    }

    public void SelectAction()
    {
      if (OnActionSelected != null)
      {
        OnActionSelected(action);
      }
    }
  }
}