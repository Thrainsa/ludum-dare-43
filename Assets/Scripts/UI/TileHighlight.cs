﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.map
{
  public class TileHighlight : MonoBehaviour
  {
    private SpriteRenderer sprite;

    private Color baseColor = Color.white;

    private const float FADE_SPEED = 0.03f;

    private readonly static Vector3 MAX_SIZE = 0.8f * new Vector3(1, 1, 1);

    private readonly static Color COLOR_GREEN = new Color(130/255f, 200/255f, 80/255f, 0.3f);
    private readonly static Color COLOR_EXPLORE = new Color(85/255f, 151/255f, 255/255f, 0.2f);
    private readonly static Color COLOR_EXPLORE_SELECTED = new Color(255/255f, 255/255f, 255/255f, 0.4f);

    public enum HighlightColor
    {
      GREEN,
      EXPLORE,
      EXPLORE_SELECTED,
      INVISIBLE
    }

    void Awake()
    {
      sprite = GetComponent<SpriteRenderer>();
      transform.localScale = Vector3.zero;
    }

    void Update()
    {
      sprite.color = baseColor + new Color(0, 0, 0, Mathf.Sin(Time.time * 4) * 0.05f);
    }

    public void Highlight(HighlightColor color, bool fadeIn = true)
    {
      switch (color)
      {
        case HighlightColor.GREEN: baseColor = COLOR_GREEN; break;
        case HighlightColor.EXPLORE: baseColor = COLOR_EXPLORE; break;
        case HighlightColor.EXPLORE_SELECTED: baseColor = COLOR_EXPLORE_SELECTED; break;
        case HighlightColor.INVISIBLE: Unhighlight(); return;
        default: baseColor = Color.white; break;
      }
      sprite.color = baseColor;
      if(fadeIn)
        StartCoroutine(FadeIn());
      else
        transform.localScale = MAX_SIZE;
    }

    public void Unhighlight()
    {
      StartCoroutine(FadeOut());
    }

    IEnumerator FadeIn()
    {
      transform.localScale = Vector3.zero;
      while (transform.localScale.magnitude < MAX_SIZE.x)
      {
        transform.localScale += FADE_SPEED * MAX_SIZE;
        yield return new WaitForEndOfFrame();
      }
      transform.localScale = MAX_SIZE;
    }

    IEnumerator FadeOut()
    {
      while (transform.localScale.magnitude > 0.05f)
      {
        transform.localScale -= FADE_SPEED * MAX_SIZE;
        yield return new WaitForEndOfFrame();
      }
      transform.localScale = Vector3.zero;
      gameObject.SetActive(false);
    }
  }

}