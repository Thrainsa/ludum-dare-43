﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ZBGE.Game;

namespace ZBGE.UI
{
  public class CharacterIconBtn : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
  {
    public delegate bool CharacterSelect(Character character, bool selected);
    public event CharacterSelect OnCharacterSelectionChanged;

    public delegate void MouseEnter(Character character);
    public event MouseEnter OnMouseEnter;

    public delegate void MouseExit(Character character);
    public event MouseExit OnMouseExit;

    public Image selectedBackground;

    public Character character;
    public Image characterIcon;
    public TextMeshProUGUI abbreviation;

    private bool selected = false;

    public void Init(Character character)
    {
      this.character = character;
      this.characterIcon.sprite = character.characterData.portrait;
      this.abbreviation.SetText(character.characterData.trait.abbreviation);
    }

    public void SelectCharacter()
    {
      SetCharacterSelected(!selected);
    }

    public void SetCharacterSelected(bool value, bool force = false)
    {
      bool selectAllowed = true;
      if (OnCharacterSelectionChanged != null)
      {
        selectAllowed = OnCharacterSelectionChanged(character, value);
      }

      if (selectAllowed || force)
      {
        PlayerManager playerManager = FindObjectOfType<PlayerManager>();
        playerManager.PlayClip(playerManager.selectSound);
        selected = value;
        selectedBackground.gameObject.SetActive(value);
      }
    }

    void IPointerEnterHandler.OnPointerEnter(PointerEventData eventData)
    {
      if (OnMouseEnter != null)
      {
        OnMouseEnter(character);
      }
    }

    void IPointerExitHandler.OnPointerExit(PointerEventData eventData)
    {
      if (OnMouseEnter != null)
      {
        OnMouseExit(character);
      }
    }
  }
}