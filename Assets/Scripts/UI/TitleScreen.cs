﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour
{
  public static bool victory = false;
  public static bool gameover = false;

  public TextMeshProUGUI startTxt;
  public GameObject victoryImg;
  public GameObject gameOverImg;

  void Start()
  {
    if(TitleScreen.victory) victoryImg.SetActive(true);
    if(TitleScreen.gameover) gameOverImg.SetActive(true);
    if(TitleScreen.victory || TitleScreen.gameover) startTxt.SetText("Restart");

    TitleScreen.victory = false;
    TitleScreen.gameover = false;
  }

  public void StartMainScene()
  {
    StartCoroutine(DoStartMainScene());
  }

  public IEnumerator DoStartMainScene()
  {
    GetComponent<AudioSource>().Play();
    yield return new WaitForSeconds(1f);
    SceneManager.LoadScene("main");
  }

  public void Quit(){
    Application.Quit();
  }

}
