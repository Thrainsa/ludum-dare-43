﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.UI
{
  public class Floating : MonoBehaviour
  {

    public float fadeDelay = 0.3f;

    public float floatingAmplitude = 0.03f;

    public float floatingSpeed = 10.0f;

    private float targetAlpha;

    private SpriteRenderer sprite;
    private Color initialColor;
    private Color transparentColor;
    private Vector3 basePosition;


    void Awake()
    {
      sprite = GetComponent<SpriteRenderer>();
      initialColor = sprite.color;
      targetAlpha = initialColor.a;
      transparentColor = new Color(initialColor.r, initialColor.g, initialColor.b, 0);
      basePosition = transform.localPosition;
    }

    void Update()
    {
      if (sprite.color.a != targetAlpha)
      {
        Color targetColor = targetAlpha == 0 ? transparentColor : initialColor;
        sprite.color = Color.Lerp(sprite.color, targetColor, Time.deltaTime / fadeDelay);
      }
      transform.localPosition = basePosition + Mathf.Sin(Time.time * floatingSpeed) * Vector3.up * floatingAmplitude;
    }

    public void SetVisible(bool visible)
    {
      sprite = GetComponent<SpriteRenderer>();
      sprite.color = visible ? initialColor : transparentColor;
      targetAlpha = visible ? initialColor.a : 0;
    }

    public void FadeIn()
    {
      targetAlpha = initialColor.a;
    }

    public void FadeOut()
    {
      targetAlpha = 0;
    }

  }

}