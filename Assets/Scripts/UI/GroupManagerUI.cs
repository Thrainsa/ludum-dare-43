﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ZBGE.Game;

namespace ZBGE.UI
{
  public class GroupManagerUI : MonoBehaviour
  {
    public CharacterIconBtn characterIconBtnPrefab;
    public Transform iconsContent;

    public CharacterInfoUI characterInfoUI;

    public GroupInfoUI groupInfoUI;

    private GroupManager groupManager;
    private List<Character> selectedCharacters = new List<Character>();
    private Dictionary<Character, CharacterIconBtn> characterButtons = new Dictionary<Character, CharacterIconBtn>();

    private bool canSelectCharacters = false;

    void Start()
    {
      groupManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
      groupManager.group.OnGroupChange += UpdateUI;
      UpdateUI(groupManager.group.Characters);
    }

    void UpdateUI(List<Character> characters)
    {
      // Disable all buttons
      foreach (var item in characterButtons.Values)
      {
        item.gameObject.SetActive(false);
      }

      // Enable or create new icons
      foreach (var item in groupManager.group.GetCharacters(CharacterSort.POWER))
      {
        if (characterButtons.ContainsKey(item))
        {
          characterButtons[item].gameObject.SetActive(true);
          characterButtons[item].transform.SetAsLastSibling();
        }
        else
        {
          characterButtons.Add(item, CreateIcon(item));
        }
      }
    }

    private CharacterIconBtn CreateIcon(Character character)
    {
      CharacterIconBtn characterBtn = GameObject.Instantiate(characterIconBtnPrefab, Vector3.zero, Quaternion.identity, iconsContent) as CharacterIconBtn;
      characterBtn.Init(character);
      characterBtn.OnCharacterSelectionChanged += CharacterSelection;
      characterBtn.OnMouseEnter += CharacterInfoShow;
      characterBtn.OnMouseExit += CharacterInfoHide;
      characterBtn.GetComponent<Button>().interactable = canSelectCharacters;
      return characterBtn;
    }

    private bool CharacterSelection(Character character, bool selected)
    {
      if (!canSelectCharacters)
      {
        return false;
      }

      if (selected)
      {
        selectedCharacters.Add(character);
      }
      else
      {
        selectedCharacters.Remove(character);
      }
      return true;
    }

    private void CharacterInfoShow(Character character)
    {
      characterInfoUI.ShowCharacter(character);
      characterInfoUI.gameObject.SetActive(true);
      groupInfoUI.gameObject.SetActive(false);
    }

    private void CharacterInfoHide(Character character)
    {
      characterInfoUI.gameObject.SetActive(false);
      groupInfoUI.gameObject.SetActive(true);
    }

    public List<Character> SelectedCharacters
    {
      get { return new List<Character>(selectedCharacters); }
    }

    public bool CanSelectCharacters
    {
      get
      {
        return canSelectCharacters;
      }
      set
      {
        canSelectCharacters = value;
        if (!canSelectCharacters)
        {
          UnselectAll();
        }
        foreach (CharacterIconBtn button in characterButtons.Values)
        {
          button.GetComponent<Button>().interactable = canSelectCharacters;
        }
      }
    }

    public void UnselectAll()
    {
      foreach (var item in characterButtons.Values)
      {
        item.SetCharacterSelected(false, true);
      }
    }
  }
}