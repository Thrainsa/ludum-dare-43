﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.data;
using ZBGE.Game;

namespace ZBGE.UI
{
  public class Compass : MonoBehaviour
  {
    public const int VISIBILITY_TURNS = 3;

    private const int ANGLE_OFFSET = 0;

    private const float MAX_OFFSET_PER_TURN = 15f;

    private const float MAX_SPEED = 10f;

    private float direction = 0;

    private int precision = 0;

    private SpriteRenderer sprite;

    private float currentSpeed = 0;

    private float nextSpeedChange = 0;

    void Start()
    {
      sprite = GetComponent<SpriteRenderer>();
    }

    public int MarkObjectiveDirection(int precision = VISIBILITY_TURNS)
    {
      this.precision = Mathf.Clamp(precision, 0, VISIBILITY_TURNS);
      RefreshOpacity();

      Objective objective = FindObjectOfType<Objective>();
      if (objective != null)
      {
        transform.up = objective.transform.position - transform.position;
        direction = transform.localEulerAngles.z;
        transform.Rotate(0, 0, Random.Range(-MAX_OFFSET_PER_TURN, MAX_OFFSET_PER_TURN));
      }
      return this.precision;
    }

    public void NewTurn()
    {
      precision--;
      RefreshOpacity();
    }

    private void RefreshOpacity()
    {
      sprite.color = new Color(1, 1, 1, Mathf.Max(0, precision) * 1f / VISIBILITY_TURNS);
    }

    void Update()
    {
      transform.Rotate(0, 0, currentSpeed * Time.deltaTime);
      if (Mathf.Abs(transform.localEulerAngles.z - direction) > MAX_OFFSET_PER_TURN * (VISIBILITY_TURNS - precision))
      {
        currentSpeed *= -1;
        transform.Rotate(0, 0, 2 * currentSpeed * Time.deltaTime);
      }

      nextSpeedChange -= Time.deltaTime;
      if (nextSpeedChange < 0)
      {
        currentSpeed = Random.Range(-MAX_SPEED, MAX_SPEED);
        nextSpeedChange = Random.Range(0, 3);
      }
    }
  }

}