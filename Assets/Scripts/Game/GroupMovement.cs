using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZBGE.data;
using ZBGE.map;

namespace ZBGE.Game
{
  public class GroupMovement : MonoBehaviour
  {

    public static float SMOOTH_TIME = 0.2f;

    public static float MAX_SPEED = 10.0f;

    public TileComponent previousTile;

    public TileComponent targetTile = null;

    private MapComponent map;

    private Vector2 currentVelocity;

    private TileComponent currentTile;

    public TileComponent CurrentTile { get { return currentTile; } }

    void Start()
    {
      map = FindObjectOfType<MapComponent>();
    }

    public bool GoTo(TileComponent tile)
    {
      if (IsReachable(tile))
      {
        GetComponent<AudioSource>().Play();
        targetTile = tile;
        return true;
      }
      return false;
    }

    public bool IsReachable(TileComponent tile)
    {
      return ReachableTiles.Contains(tile);
    }

    public List<TileComponent> ReachableTiles
    {
      get
      {
        List<TileComponent> tiles = map.GetNeighbors(currentTile);
        // tiles.Add(currentTile);
        return tiles;
      }
    }

    void FixedUpdate()
    {
      if (targetTile != null)
      {
        transform.position = Vector2.SmoothDamp(transform.position, targetTile.GetPosition(), ref currentVelocity, SMOOTH_TIME, MAX_SPEED);
        if (Vector2.Distance(transform.position, targetTile.GetPosition()) < 0.03f)
        {
          SetCurrentTile(targetTile);
          targetTile = null;
        }
      }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
      // Initial state
      if (currentTile == null)
      {
        TileComponent tile = collider.GetComponent<TileComponent>();
        if (tile != null)
        {
          SetCurrentTile(tile);

          // Make all neighbor tiles visible & safe
          foreach (TileComponent neighbor in map.GetNeighbors(tile))
          {
            neighbor.Visited = true;
            neighbor.eventDrawn = true;
          }
        }
      }
    }

    public void SetCurrentTile(TileComponent tile)
    {
      previousTile = currentTile;
      currentTile = tile;
      currentTile.Visited = true;
      foreach (TileComponent neighbor in map.GetNeighbors(currentTile))
      {
        neighbor.Visited = true;
      }
      transform.position = currentTile.GetPosition();
    }
  }
}