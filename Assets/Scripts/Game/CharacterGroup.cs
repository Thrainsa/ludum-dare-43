using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZBGE.data;

namespace ZBGE.Game
{

  public class CharacterGroup
  {

    public delegate void GroupChange(List<Character> characters);
    public event GroupChange OnGroupChange;

    private List<Character> characters = new List<Character>();

    public int rations = 0;

    internal void AddCharacter(CharacterData newCharacter)
    {
      characters.Add(new Character(newCharacter));
      if (OnGroupChange != null)
      {
        OnGroupChange(Characters);
      }
    }

    internal bool LostCharacter(Character character)
    {
      bool success = characters.Remove(character);
      if(OnGroupChange != null)
        OnGroupChange(Characters);
      return success;
    }

    public void GainCharacter(Character character)
    {
      characters.Add(character);
      OnGroupChange(Characters);
    }

    public List<Character> GetCharacters(CharacterSort sort = CharacterSort.ANY, CharacterFilter filter = CharacterFilter.ALL)
    {
      List<Character> charactersView = new List<Character>(characters);
      if (sort == CharacterSort.POWER)
      {
        charactersView.Sort((c1, c2) =>
        {
          int result = c2.characterData.trait.power.CompareTo(c1.characterData.trait.power);
          if (result == 0)
          {
            result = c2.characterData.trait.title.CompareTo(c1.characterData.trait.title);
          }
          return result;
        });
      }
      return charactersView;
    }

    public List<Character> Characters
    {
      get { return new List<Character>(characters); }
    }

    public int CharacterCount
    {
      get { return characters.Count; }
    }

    public void AddRations(int gain)
    {
      this.rations += gain;
    }

    public void EraseGroup(List<Character> characters) {
      this.characters = characters;
    }
  }
}