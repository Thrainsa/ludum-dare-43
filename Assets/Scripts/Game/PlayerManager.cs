﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using ZBGE.data;
using ZBGE.map;
using ZBGE.UI;

namespace ZBGE.Game
{
  public class PlayerManager : MonoBehaviour
  {
    public delegate void OnGroupMove(GroupMovement group);
    public event OnGroupMove OnMoveEvent;

    public delegate void OnSelectTile(TileComponent tile);
    public event OnSelectTile OnSelectTileEvent;

    public GroupManager selectedGroup = null;

    public AudioClip clickSound;

    public AudioClip alertSound;
    
    public AudioClip click2Sound;

    public AudioClip selectSound;

    private bool canMoveGroup;
    private bool canSelectTile;
    private UIManager uIManager;

    void Start()
    {
      uIManager = FindObjectOfType<UIManager>();
    }

    private List<TileComponent> highlightedTiles = null;

    public bool CanMoveGroup
    {
      set
      {
        canMoveGroup = value;

        // Select first available group by default
        if (canMoveGroup && selectedGroup == null)
        {
          GroupManager group = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
          if (group != null) OnGroupClick(group);
        }
        else if (!canMoveGroup)
        {
          Unselect(selectedGroup);
        }

        // Highlight walkable tiles
        if (canMoveGroup)
        {
          if (highlightedTiles != null) HighlightTiles(highlightedTiles, TileHighlight.HighlightColor.INVISIBLE);
          highlightedTiles = selectedGroup.ReachableTiles;
          HighlightTiles(highlightedTiles, TileHighlight.HighlightColor.GREEN);
        }
        else if (highlightedTiles != null)
        {
          HighlightTiles(highlightedTiles, TileHighlight.HighlightColor.INVISIBLE);
          highlightedTiles = null;
        }

      }
    }

    public bool CanSelectTile
    {
      set
      {
        canSelectTile = value;

        // Select group
        GroupManager group = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
        if (group != null) OnGroupClick(group);

        // Highlight walkable tiles
        if (canSelectTile)
        {
          if (highlightedTiles != null) HighlightTiles(highlightedTiles, TileHighlight.HighlightColor.INVISIBLE);
          highlightedTiles = group.ReachableTiles;
          HighlightTiles(highlightedTiles, TileHighlight.HighlightColor.EXPLORE);
        }
        else if (highlightedTiles != null)
        {
          HighlightTiles(highlightedTiles, TileHighlight.HighlightColor.INVISIBLE);
          highlightedTiles = null;
        }

      }
    }

    private void HighlightTiles(List<TileComponent> tiles, TileHighlight.HighlightColor color, bool fadeIn = true)
    {
      if (tiles != null)
      {
        tiles.ForEach(tile => tile.Highlight(color, fadeIn));
      }
    }

    public void ExecuteAction(Action action, Character[] characters = null)
    {
      GroupManager group = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
      action.Execute(uIManager, group, selectedGroup.group.Characters, group.movement.CurrentTile);
    }

    void OnTileClick(TileComponent tile)
    {
      if (selectedGroup != null)
      {
        GroupMovement groupMovement = selectedGroup.GetComponent<GroupMovement>();
        if (canMoveGroup)
        {
          bool success = groupMovement.GoTo(tile);
          if (success)
          {
            OnMoveEvent(groupMovement);
          }
        }
        else if (canSelectTile)
        {
          if (groupMovement.IsReachable(tile))
          {
            HighlightTiles(highlightedTiles, TileHighlight.HighlightColor.EXPLORE, false);
            tile.Highlight(TileHighlight.HighlightColor.EXPLORE_SELECTED, false);
            OnSelectTileEvent(tile);
          }
        }
      }
    }

    void OnGroupClick(GroupManager group)
    {
      if (group != null)
      {
        Unselect(selectedGroup);
        selectedGroup = group;
        selectedGroup.SetSelected(true);
      }
    }

    void Unselect(GroupManager group)
    {
      if (selectedGroup != null)
      {
        selectedGroup.SetSelected(false);
        selectedGroup = null;
      }
    }

    void Update()
    {
      // Exit
      if (Input.GetKeyDown(KeyCode.Escape))
      {
        BackToMenu();
      }

      // Movement management
      if (Input.GetMouseButtonDown(0) && (canMoveGroup || canSelectTile))
      {
        RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

        if (hits.Length > 0)
        {
          // Verify UI collision
          foreach (RaycastHit2D hit in hits)
          {
            if (hit.collider.gameObject.layer == 5 /* UI Layer*/)
            {
              return;
            }
          }

          // Priority to groups if we are not selecting tiles
          bool groupClicked = false;
          if (canSelectTile)
          {
            foreach (RaycastHit2D hit in hits)
            {
              GroupManager group = hit.collider.GetComponent<GroupManager>();
              if (group != null)
              {
                OnGroupClick(group);
                groupClicked = true;
              }
            }
          }

          // Check tiles otherwise
          if (!groupClicked)
          {
            foreach (RaycastHit2D hit in hits)
            {
              TileComponent tile = hit.collider.GetComponent<TileComponent>();
              if (tile != null) OnTileClick(tile);
            }
          }
        }

      }

    }
    public void ReloadScene()
    {
      StartCoroutine(DoReloadScene());
    }
    IEnumerator DoReloadScene()
    {
      yield return new WaitForSeconds(3);
      SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void BackToMenu()
    {
      StartCoroutine(DoBackToMenu());
    }
    IEnumerator DoBackToMenu()
    {
      yield return new WaitForSeconds(3);
      SceneManager.LoadScene("title");
    }

    public void PlayClip(AudioClip clip)
    {
      AudioSource source = GetComponent<AudioSource>();
      source.clip = clip;
      source.Play();
    }

  }

}