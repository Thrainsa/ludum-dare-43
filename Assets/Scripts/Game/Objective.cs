﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using ZBGE.UI;

namespace ZBGE.Game
{


  public class Objective : MonoBehaviour
  {
    public delegate void OnGroupCollision(GroupMovement group);

    public event OnGroupCollision OnGroupCollisionEvent;

    void Start()
    {
      ZBGE.UI.Compass compass = FindObjectOfType<ZBGE.UI.Compass>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
      // Reload scene upon victory
      GroupMovement group = collider.GetComponent<GroupMovement>();
      if (group != null)
      {
        OnGroupCollisionEvent(group);
      }
    }

  }

}