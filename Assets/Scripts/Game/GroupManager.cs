﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.data;
using ZBGE.map;
using ZBGE.UI;

namespace ZBGE.Game
{
  public class GroupManager : MonoBehaviour
  {
    public CharacterGroup group = new CharacterGroup();

    public Floating selectedIndicator;

    public GroupMovement movement;

    public ZBGE.UI.Compass compass;

    public void Start()
    {
      movement = GetComponent<GroupMovement>();
      if(selectedIndicator)
        selectedIndicator.SetVisible(false);
    }

    public void InitializeGroup(Database database, int startSacrifices, int bonusFood)
    {
      int generated = 0;
      for (int i = 0; i < database.mandatoryStartTraits.Length; i++)
      {
        CharacterData character = database.GenerateCharacter(database.mandatoryStartTraits[i]);
        group.AddCharacter(character);
        generated++;
      }

      while (generated < database.startCharacters - startSacrifices)
      {
        CharacterTrait trait = database.allowedStartTraits[Random.Range(0, database.allowedStartTraits.Length)];
        CharacterData character = database.GenerateCharacter(trait);
        group.AddCharacter(character);
        generated++;
      }
      group.AddRations(bonusFood);
    }

    public void SetSelected(bool selected)
    {
      if(selectedIndicator != null) {
        if (selected)
        {
          selectedIndicator.FadeIn();
        }
        else
        {
          selectedIndicator.FadeOut();
        }
      }
    }

    public TileComponent CurrentTile
    {
      get { return movement.targetTile ? movement.targetTile : movement.CurrentTile; }
    }

    public List<TileComponent> ReachableTiles
    {
      get { return movement.ReachableTiles; }
    }

    public int GetCombatValue()
    {
      return group.Characters.OrderByDescending(c => c.GetCombatBonus())
        .Take(5).Sum(c => c.GetCombatBonus());
    }

    public int GetExplorationValue()
    {
      return group.Characters.OrderByDescending(c => c.GetExplorationBonus())
        .Take(5).Sum(c => c.GetExplorationBonus());
    }
  }
}