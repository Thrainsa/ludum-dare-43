using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Event Trigger", fileName = "ETrigger_")]
  public class EventTrigger : ScriptableObject
  {

    [Header("Hook")]
    public EventTriggerHook hook;

    [Header("Conditions")]
    public TerrainType terrainType;

    public bool Matches(EventContext context)
    {
      if (hook != context.hook) return false;
      if (terrainType != null && !terrainType.title.Equals(context.terrainType.title)) return false;
      return true;
    }

  }

}
