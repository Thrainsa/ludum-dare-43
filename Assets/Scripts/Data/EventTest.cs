using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;

using NaughtyAttributes;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Event Test", fileName = "ETest")]
  public class EventTest : ScriptableObject
  {

    [Header("Difficulty")]

    public int testDifficulty;

    [Header("1d20 + Bonus:")]

    public bool useCombatBonus;
    
    public bool useExplorationBonus;

    public bool Resolve(GroupManager groupManager)
    {
      // List<Character> characters;
      // if (useCombatBonus) {
      //   characters = groupManager.group.Characters.OrderByDescending(c => c.GetCombatBonus()).Take(5).ToList();
      // } else {
      //   characters = groupManager.group.Characters.OrderByDescending(c => c.GetExplorationBonus()).Take(5).ToList();
      // }
      // return Resolve(characters);
      int bonus = 0;
      if (useCombatBonus) {
        bonus = groupManager.GetCombatValue();
      }
      if (useExplorationBonus) {
        bonus = groupManager.GetExplorationValue();
      }
      return ThrowTest(testDifficulty, bonus);
    }

    public bool Resolve(List<Character> characters)
    {
      int bonus = 0;
      if (useCombatBonus) {
        bonus = characters.Sum(c => c.GetCombatBonus());
      }
      if (useExplorationBonus) {
        bonus = characters.Sum(c => c.GetExplorationBonus());
      }
      return ThrowTest(testDifficulty, bonus);
    }

    private bool ThrowTest(int difficulty, int bonus) 
    {
      int die = Random.Range(1, 20);
      int modifiedDie = die + bonus;
      bool success = modifiedDie > difficulty;
      Debug.Log("Die thrown (difficulty " + difficulty + "): " + die + " + " + bonus + " bonus = " + (success?"SUCCESS":"FAILURE"));

      return success;

    }

  }

}
