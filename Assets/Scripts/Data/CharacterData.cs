using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.data
{

  public class CharacterData
  {

    public CharacterIdentity identity;

    public Sprite portrait;

    public CharacterTrait trait;

    public CharacterData(CharacterIdentity identity, Sprite portrait, CharacterTrait trait)
    {
      this.identity = identity;
      this.portrait = portrait;
      this.trait = trait;
    }
  }

}