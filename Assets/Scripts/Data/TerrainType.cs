using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Terrain Type", fileName = "Terrain_")]
  public class TerrainType : ScriptableObject
  {
    private const int averageFood = 4;
    private float[] bonusRange = { 0f, 0.25f, 0.75f, 1.25f, 1.75f, 2f };

    public string title;

    public Sprite[] sprites;

    [Range(-2, 2)]
    public int foodSearchBonus;

    [Range(-2, 2)]
    public int recoveryBonus;

    [Range(1, 3)]
    public int dangerLevel;

#if UNITY_EDITOR
    public void OnInspectorGUI()
    {
      title = EditorGUILayout.TextField("Title", title);
    }
#endif

    public Sprite GetRandomSprite()
    {
      return sprites[Random.Range(0, sprites.Length)];
    }

    public int GenerateInitialFood()
    {
      return Random.Range(Mathf.CeilToInt(averageFood * bonusRange[foodSearchBonus + 2]),
                          Mathf.CeilToInt(averageFood * bonusRange[foodSearchBonus + 3]) + 1);
    }
  }

}