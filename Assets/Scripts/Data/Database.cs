using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NaughtyAttributes;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Database")]
  public class Database : ScriptableObject
  {

    [Header("Constants")]
    public int startCharacters;
    public CharacterTrait[] mandatoryStartTraits;
    public CharacterTrait[] allowedStartTraits;

    [Header("Data lists")]
    public CharacterTrait[] traits;

    public Event[] events;


    public List<Event> GetMatchingEvents(EventContext context)
    {
      List<Event> matches = new List<Event>();
      foreach (Event evt in events)
      {
        foreach (EventTrigger trigger in evt.trigger)
        {
          if (trigger.Matches(context))
          {
            matches.Add(evt);
            break;
          }
        }
      }
      return matches;
    }

    public CharacterData GenerateCharacter(CharacterTrait forceTrait = null)
    {
      CharacterTrait trait = forceTrait;
      if (trait == null) trait = traits[Random.Range(0, traits.Length)];

      CharacterIdentity identity = trait.possibleIdentities[Random.Range(0, trait.possibleIdentities.Length)];
      CharacterPortrait portrait = trait.GetRandomPortrait(identity.gender);
      
      return new CharacterData(identity, portrait.sprite, trait);
    }

  }
}