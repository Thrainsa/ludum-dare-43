using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.data
{

  public enum EffectType
  {
    DEATH,
    RATION,
    SANITY,
    STAMINA,
    EVENT
  }

}
