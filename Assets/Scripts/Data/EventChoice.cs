using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NaughtyAttributes;
using System;
using ZBGE.Game;
using ZBGE.map;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Event Choice", fileName = "EChoice_")]
  public class EventChoice : ScriptableObject
  {

    public string text;

    public EventTest test;

    [Header("SUCCESS")]

    [TextArea]
    public string outcomeSuccessText;

    [ReorderableList] public EventOutcome[] outcomesSuccess;

    [Header("FAILURE")]
    [TextArea]
    public string outcomeFailureText;

    [ReorderableList] public EventOutcome[] outcomesFailure;

    public void ApplyOutcomes(Event currentEvent, PlayerManager playerManager, GroupManager groupManager, bool success)
    {
      EventOutcome[] outcomes = success ? outcomesSuccess : outcomesFailure;

      EventOutcome.ApplyOutcomes(currentEvent, playerManager, groupManager, outcomes);
    }
  }
}
