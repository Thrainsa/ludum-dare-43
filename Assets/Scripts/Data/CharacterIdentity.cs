using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Character Identity", fileName = "Identity_")]
  public class CharacterIdentity : ScriptableObject
  {

    public string title;

    public Gender gender;

  }

  public enum Gender
  {
    MALE,
    FEMALE
  }

}