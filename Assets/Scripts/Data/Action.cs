using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;
using ZBGE.map;
using ZBGE.UI;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Action", fileName = "Action_")]
  public class Action : ScriptableObject
  {

    public const string ACTION_EAT = "Eat";
    public const string ACTION_EXPLORE = "Explore";
    public const string ACTION_LOOK_FOR_FOOD = "Look for food";
    public const string ACTION_REST = "Rest";
    public const string ACTION_CANCEL = "Cancel";
    public const string ACTION_CONFIRM = "Confirm";
    public const string ACTION_END_OF_TURN = "End turn";
    public const string ACTION_DIRECTIONS = "Directions";
    public string title;

    [TextArea]
    public string description;

    public bool alwaysAvailable = false;

    public void Execute(UIManager uiManager, GroupManager group, List<Character> selectedCharacters, TileComponent selectedTile)
    {
      switch (title)
      {
        case ACTION_REST:
          ExecuteRest(uiManager, selectedCharacters);
          break;
        case ACTION_LOOK_FOR_FOOD:
          ExecuteLookForFood(uiManager, group, selectedCharacters);
          break;
        case ACTION_DIRECTIONS:
          ExecuteDirections(uiManager, selectedCharacters);
          break;
        case ACTION_EXPLORE:
          SendExploreExpedition(uiManager, selectedCharacters, selectedTile);
          break;
        default:
          Debug.LogError("Unknown action " + title);
          break;
      }
    }

    public void SendExploreExpedition(UIManager uiManager, List<Character> selectedCharacters, TileComponent selectedTile)
    {
      GroupManager exploreGroupManager = GameObject.FindGameObjectWithTag("ExploreGroup").GetComponent<GroupManager>();
      exploreGroupManager.group.EraseGroup(selectedCharacters);
      exploreGroupManager.movement.SetCurrentTile(selectedTile);
    }

    public void ExecuteRest(UIManager uiManager, List<Character> selectedCharacters)
    {
      selectedCharacters.ForEach(c => c.AddSanity(1));
      uiManager.ShowInfoPopup("Every one enjoy the rest. \r\n +1 sanity.");
    }

    public void ExecuteLookForFood(UIManager uiManager, GroupManager group, List<Character> selectedCharacters)
    {
      selectedCharacters.ForEach(c => c.AddStamina(-1));

      int foodFound = group.CurrentTile.HarvestFood(selectedCharacters.Count * 4);
      group.group.AddRations(foodFound);
      if (foodFound > 0)
      {
        uiManager.ShowInfoPopup("Your team manage to get " + foodFound + " rations.");
      }
      else
      {
        uiManager.ShowInfoPopup("After looking for hours, no one found food. Too bad.");
      }
    }

    public void ExecuteDirections(UIManager uiManager, List<Character> selectedCharacters)
    {
    /*  int success = 0;
      selectedCharacters.ForEach(c => success += c.characterData.trait.explorationBonus);*/

      ZBGE.UI.Compass compass = FindObjectOfType<ZBGE.UI.Compass>();
      int turnsActive = compass.MarkObjectiveDirection();
      uiManager.ShowInfoPopup("The president orders you to go this way. Compass is active for " + turnsActive + " turns.");
    }

  }

}