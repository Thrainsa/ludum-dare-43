using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NaughtyAttributes;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Character Trait", fileName = "CharacterTrait_")]
  public class CharacterTrait : ScriptableObject
  {

    public string title;

    public string abbreviation;

    [Range(1, 4)]
    public int power;

    public int combatBonus;
    public bool combatStaminaBonus;

    public int explorationBonus;
    public bool explorationSanityBonus;

    public Action[] possibleActions;

    public CharacterPortrait[] possiblePortraits;

    public CharacterIdentity[] possibleIdentities;

    public bool needToSurvive;
    public bool bodyguard;

    public CharacterPortrait GetRandomPortrait(Gender preferredGender)
    {
      List<CharacterPortrait> compatiblePortraits = new List<CharacterPortrait>();
      foreach (CharacterPortrait portrait in possiblePortraits)
      {
        if (portrait.gender == preferredGender)
        {
          compatiblePortraits.Add(portrait);
        }
      }
      if (compatiblePortraits.Count == 0)
      {
        Debug.LogWarning("No portrait matching gender " + preferredGender + " for trait " + this.name);
        foreach (CharacterPortrait portrait in possiblePortraits) compatiblePortraits.Add(portrait);
      }
      return compatiblePortraits[Random.Range(0, compatiblePortraits.Count)];
    }

  }

}