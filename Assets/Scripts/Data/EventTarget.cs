using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.data
{

  public enum TargetSelection
  {
    RANDOM,
    TILE,
    GROUP,
    LESS_STAMINA,
    LESS_SANITY
  }

}
