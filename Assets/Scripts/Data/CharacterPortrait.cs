using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NaughtyAttributes;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Character Portrait", fileName = "Portrait_")]
  public class CharacterPortrait : ScriptableObject
  {

    [ShowAssetPreview]
    public Sprite sprite;

    public Gender gender;

  }

}