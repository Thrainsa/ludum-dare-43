using NaughtyAttributes;
using UnityEngine;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "ActionList", fileName = "ActionList_")]
  public class ActionList : ScriptableObject
  {

    [ReorderableList]
    public Action[] actions;

  }

}