using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

using NaughtyAttributes;
using ZBGE.Game;
using ZBGE.map;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Event Outcome", fileName = "EOutcome_")]
  public class EventOutcome : ScriptableObject
  {
    private List<EffectType> HAS_EFFECT_MIN_MAX = new List<EffectType> { EffectType.RATION, EffectType.SANITY, EffectType.STAMINA };
    private List<EffectType> NO_TARGET_NEEDED = new List<EffectType> { EffectType.EVENT };

    [Header("Effect")]
    public EffectType effectType;

    [ShowIf("hasEffectMinMax")]
    public Vector2Int effectMinMax;

    [ShowIf("isEvent")]
    public bool goBackToPreviousTile;
    [ShowIf("isEvent")]
    public bool rearmEvent;
    [ShowIf("isEvent")]
    public Event replaceEvent;

    [Header("Target")]

    [HideIf("noTargetNeeded")]
    public TargetSelection selectionMode;

    [HideIf("noTargetNeeded")]
    public Vector2Int targetsMinMax;
    // [HideIf("noTargetNeeded")]
    [HideInInspector]
    public Vector2Int targetsMinMaxPercent;

    public static void ApplyOutcomes(Event currentEvent, PlayerManager playerManager, GroupManager groupManager, EventOutcome[] outcomes, bool scouting = false)
    {
      TileComponent eventTile = groupManager.movement.CurrentTile;
      Event newEvent = null;
      bool keepEvent = false;
      foreach (var outcome in outcomes)
      {
        outcome.Apply(playerManager, groupManager);
        if (outcome.rearmEvent)
        {
          keepEvent = true;
        }
        if (outcome.replaceEvent)
        {
          newEvent = outcome.replaceEvent;
        }
      }

      if (keepEvent || scouting)
      {
        eventTile.randomEvent = currentEvent;
      }
      else if (newEvent != null)
      {
        eventTile.randomEvent = newEvent;
      }
      else
      {
        eventTile.randomEvent = null;
      }
    }

    public void Apply(PlayerManager playerManager, GroupManager groupManager)
    {
      switch (effectType)
      {
        case EffectType.RATION:
          GainRations(playerManager, groupManager);
          break;
        case EffectType.STAMINA:
          GainStamina(playerManager, groupManager);
          break;
        case EffectType.SANITY:
          GainSanity(playerManager, groupManager);
          break;
        case EffectType.DEATH:
          Kill(playerManager, groupManager);
          break;
        case EffectType.EVENT:
          Event(playerManager, groupManager);
          break;
        default:
          Debug.LogWarning("Effet non géré: " + effectType);
          break;
      }
    }

    private void Event(PlayerManager playerManager, GroupManager groupManager)
    {
      if (goBackToPreviousTile)
      {
        groupManager.movement.SetCurrentTile(groupManager.movement.previousTile);
      }
    }

    private void Kill(PlayerManager playerManager, GroupManager groupManager)
    {
      List<Character> characters = GetTargets(groupManager);

      characters.Sort((c1, c2) => c1.characterData.trait.needToSurvive.CompareTo(c2.characterData.trait.needToSurvive));
      characters.ForEach(c =>
      {
        // Body guard protect the president
        if (c.characterData.trait.needToSurvive)
        {
          var survivors = groupManager.group.Characters.Except(characters).ToList();
          Character bg = survivors.FirstOrDefault(item => item.characterData.trait.bodyguard);
          if (bg != null)
          {
            groupManager.group.LostCharacter(bg);
          }
          else
          {
            groupManager.group.LostCharacter(c);
          }
        }
        else
        {
          groupManager.group.LostCharacter(c);
        }
      });
    }

    private void GainRations(PlayerManager playerManager, GroupManager groupManager)
    {
      int gain = Random.Range(effectMinMax[0], effectMinMax[1] + 1);
      switch (selectionMode)
      {
        case TargetSelection.GROUP:
          groupManager.group.AddRations(gain);
          break;
        case TargetSelection.TILE:
          groupManager.movement.CurrentTile.foodAvailable += gain;
          break;
        default:
          Debug.LogWarning("Target " + selectionMode + ", can't gain rations => Outcome not properly configured");
          break;
      }
    }

    private void GainStamina(PlayerManager playerManager, GroupManager groupManager)
    {
      int gain = Random.Range(effectMinMax[0], effectMinMax[1] + 1);
      List<Character> characters = GetTargets(groupManager);

      characters.ForEach(c => c.AddStamina(gain));
    }

    private void GainSanity(PlayerManager playerManager, GroupManager groupManager)
    {
      int gain = Random.Range(effectMinMax[0], effectMinMax[1] + 1);
      List<Character> characters = GetTargets(groupManager);

      characters.ForEach(c => c.AddSanity(gain));
    }

    private List<Character> GetTargets(GroupManager groupManager)
    {
      List<Character> characters = new List<Character>();
      int targetCount = Random.Range(targetsMinMax[0], Mathf.Min(targetsMinMax[1], groupManager.group.Characters.Count) + 1);
      System.Random rnd = new System.Random();

      switch (selectionMode)
      {
        case TargetSelection.RANDOM:
          characters = groupManager.group.Characters.OrderBy(x => rnd.Next()).Take(targetCount).ToList();
          break;
        case TargetSelection.LESS_STAMINA:
          characters = groupManager.group.Characters.OrderBy(c => c.stamina * 10 + rnd.Next() % 10).Take(targetCount).ToList();
          break;
        case TargetSelection.LESS_SANITY:
          characters = groupManager.group.Characters.OrderBy(c => c.sanity * 10 + rnd.Next() % 10).Take(targetCount).ToList();
          break;
        case TargetSelection.GROUP:
          characters.AddRange(groupManager.group.Characters);
          break;
        case TargetSelection.TILE:
          Debug.LogWarning("Target TILE, can't select characters => Outcome not properly configured");
          break;
        default:
          Debug.LogWarning("Target Unknown: " + selectionMode);
          break;
      }

      return characters;
    }

    private bool isEvent()
    {
      return effectType == EffectType.EVENT;
    }

    private bool hasEffectMinMax()
    {
      return HAS_EFFECT_MIN_MAX.Contains(effectType);
    }

    private bool noTargetNeeded()
    {
      return NO_TARGET_NEEDED.Contains(effectType);
    }
  }

}
