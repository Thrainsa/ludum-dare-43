using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NaughtyAttributes;

namespace ZBGE.data
{

  [CreateAssetMenu(menuName = "Events database")]
  public class Events : ScriptableObject
  {
    public EventChoice[] choices;


  }
}