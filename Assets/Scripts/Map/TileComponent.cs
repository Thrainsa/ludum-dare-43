﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.data;

namespace ZBGE.map
{
  public class TileComponent : MonoBehaviour
  {
    [Header("Terrain configuration")]
    public TerrainType terrainType;
    public bool canSpawnObjective;
    public ZBGE.data.Event randomEvent;

    [Header("State")]
    public Sprite shadowSprite;
    public int i;
    public int j;
    public SpriteRenderer objectiveSprite;

    public int foodAvailable;

    public GameObject fog;
    public TileHighlight highlight;


    public delegate ZBGE.data.Event RequestEvent(EventContext context);
    public event RequestEvent OnRequestEvent;

    private Sprite originalSprite;
    private SpriteRenderer spriteRenderer;
    private Dictionary<CharacterData, int> lostCharacters = new Dictionary<CharacterData, int>();
    private bool isVisited = false;
    public bool eventDrawn = false;
    public Vector2Int IJ
    {
      get
      {
        return new Vector2Int(i, j);
      }
    }

    public void Awake()
    {
      if (transform.localScale.x < 0.9f) transform.localScale = Vector3.one;

      if (fog == null)
      {
        Debug.LogError("Tile has no Fog object");
      }
      else
      {
        fog.SetActive(true);
        spriteRenderer = GetComponent<SpriteRenderer>();
        originalSprite = spriteRenderer.sprite;
        spriteRenderer.sprite = shadowSprite;
      }
      highlight.gameObject.SetActive(false);
    }

    public void Start()
    {
      this.foodAvailable = terrainType.GenerateInitialFood();
    }

    public ZBGE.data.Event GetEvent(EventContext context)
    {
      if (randomEvent == null && !Visited)
      {
        context.terrainType = terrainType;
        randomEvent = OnRequestEvent(context);
      }
      return randomEvent;
    }

    public Vector2 GetPosition()
    {
      return transform.position;
    }

    public void AddLostCharacter(CharacterData character, int currentTurn)
    {
      lostCharacters.Add(character, currentTurn);
    }

    public List<CharacterData> GetLostCharacters(int currentTurn)
    {
      List<CharacterData> characters = new List<CharacterData>();
      foreach (CharacterData character in lostCharacters.Keys)
      {
        // TODO Kill if too many turns passed
        characters.Add(character);
      }
      return characters;
    }

    public int HarvestFood(int maxFoodToGet)
    {
      int food = Mathf.Min(maxFoodToGet, this.foodAvailable);
      this.foodAvailable -= food;
      return food;
    }

    public void Highlight(TileHighlight.HighlightColor color, bool fadeIn = true)
    {
      highlight.gameObject.SetActive(true);
      highlight.Highlight(color, fadeIn);
    }

    public void Unhighlight()
    {
      highlight.Unhighlight();
    }

    public bool Visited
    {
      get
      {
        return isVisited;
      }

      set
      {
        isVisited = value;
        if (fog != null)
        {
          if (isVisited)
          {
            StartCoroutine(ToggleFogSmoothly(true));
          }
          else
          {
            StartCoroutine(ToggleFogSmoothly(false));
          }
        }
      }

    }

    private IEnumerator ToggleFogSmoothly(bool stop)
    {
      ParticleSystem fogParticles = fog.GetComponent<ParticleSystem>();
      if (stop)
      {
        fogParticles.Stop();
        spriteRenderer.sprite = originalSprite;
        if (objectiveSprite != null)
        {
          objectiveSprite.color = Color.white;
        }
        if (Time.timeSinceLevelLoad > 2f)
        {
          yield return new WaitForSeconds(2f);
        }
        fogParticles.gameObject.SetActive(false);
      }
      else
      {
        fogParticles.Play();
        fog.gameObject.SetActive(true);
        spriteRenderer.sprite = shadowSprite;
      }
    }

    public void RefreshSprite()
    {
      SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
      if (canSpawnObjective)
      {
        transform.localScale = new Vector3(0.8f, 0.8f);
      }
      else if (transform.localScale.x < 0.9f)
      {
        transform.localScale = Vector3.one;
      }
      if (terrainType != null)
      {
        spriteRenderer.sprite = terrainType.GetRandomSprite();
      }
      else
      {
        Debug.LogWarning("Tile " + name + " has no terrain type");
      }
    }
  }

}