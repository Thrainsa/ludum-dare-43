﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;
using ZBGE.data;
using System;

namespace ZBGE.map
{

  public class MapComponent : MonoBehaviour
  {
    public Database database;

    public Objective objectivePrefab;

    private Vector2Int[] neighborOffsets = new Vector2Int[] {
      new Vector2Int(0, 1),
      new Vector2Int(1, 0),
      new Vector2Int(1, -1),
      new Vector2Int(0, -1),
      new Vector2Int(-1, 0),
      new Vector2Int(-1, 1)
    };

    private Dictionary<int, TileComponent> tiles = new Dictionary<int, TileComponent>();

    private Objective objective;

    void Start()
    {
      // Attach & initialize children tiles
      List<TileComponent> availableObjectiveTiles = new List<TileComponent>();
      foreach (Transform row in transform)
      {
        foreach (Transform tileTransform in row)
        {
          TileComponent tile = tileTransform.GetComponent<TileComponent>();
          if (tile != null)
          {
            AddTile(tile);
            if (tile.canSpawnObjective)
            {
              availableObjectiveTiles.Add(tile);
            }
          }
        }
      }

      // Spawn objective
      if (availableObjectiveTiles.Count > 0)
      {
        TileComponent objectiveTile = availableObjectiveTiles[UnityEngine.Random.Range(0, availableObjectiveTiles.Count)];
        objective = Instantiate(objectivePrefab, objectiveTile.transform.position, Quaternion.identity, objectiveTile.transform);
        SpriteRenderer objectiveSprite =  objective.transform.Find("Sprite").GetComponent<SpriteRenderer>();
        objectiveSprite.color = Color.clear;
        objectiveTile.objectiveSprite = objectiveSprite;
      }
      else
      {
        Debug.LogWarning("No objective tile found, the game will be impossible to beat");
      }
    }

    private void AddTile(TileComponent tile)
    {
      this.tiles.Add(toHash(tile.i, tile.j), tile);
      tile.OnRequestEvent += OnRequestEvent;
    }

    private data.Event OnRequestEvent(EventContext context)
    {
      List<data.Event> events = database.GetMatchingEvents(context);
      if (events.Count > 0)
        return events[UnityEngine.Random.Range(0, events.Count)];
      else
        return null;
    }

    public TileComponent GetTile(Vector2Int coords)
    {
      return GetTile(coords.x, coords.y);
    }

    public TileComponent GetTile(int i, int j)
    {
      int hash = toHash(i, j);
      return tiles.ContainsKey(hash) ? tiles[hash] : null;
    }

    public TileComponent GetNeighbor(TileComponent tile, Direction direction)
    {
      return GetNeighbor(tile, neighborOffsets[(int)direction]);
    }

    public TileComponent GetNeighbor(TileComponent tile, Vector2Int offset)
    {
      return GetTile(tile.IJ + offset);
    }

    public List<TileComponent> GetNeighbors(TileComponent tile)
    {
      Vector2Int tileIj = tile.IJ;
      List<TileComponent> neighbors = new List<TileComponent>();
      foreach (Vector2Int neighorOffset in neighborOffsets)
      {
        TileComponent neighbor = GetTile(tileIj + neighorOffset);
        if (neighbor != null)
        {
          neighbors.Add(neighbor);
        }
      }
      return neighbors;
    }

    private int toHash(int i, int j)
    {
      return i * 100000 + j;
    }

    public Objective Objective
    {
      get { return objective; }
    }

  }

}