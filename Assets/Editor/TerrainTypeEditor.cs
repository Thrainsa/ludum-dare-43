using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

using ZBGE.map;

namespace ZBGE.data
{

  [CustomEditor(typeof(TerrainType))]
  [CanEditMultipleObjects]
  public class TerrainTypeEditor : Editor
  {

    TerrainType terrainType;

    public void OnEnable()
    {
      terrainType = (TerrainType)target;
    }

    public override void OnInspectorGUI()
    {
      base.OnInspectorGUI();

      GUILayout.Label("Actions", EditorStyles.boldLabel);
      if (GUILayout.Button("Apply to selected tiles")) {
        foreach (GameObject gameObject in Selection.gameObjects) {
          TileComponent tile = gameObject.GetComponent<TileComponent>();
          SerializedObject tileSer = new SerializedObject(tile);
          Undo.RegisterCompleteObjectUndo(tile, "Undo tile update");
          if (tile != null) {
            tile.terrainType = terrainType;
            tile.RefreshSprite();

            // XXX Otherwise terrainType change is not persisted
            SerializedProperty terrainTypeProp = tileSer.FindProperty("terrainType");
            terrainTypeProp.objectReferenceValue = terrainType;
            tileSer.ApplyModifiedProperties();
          }
        }
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
      }
      GUILayout.Label("(Tip: press the little lock icon to make sure");
      GUILayout.Label("the inspector will keep this active while you select tiles)");
    }
  }

}