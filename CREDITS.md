SACRIFICE NOW

Made by Eagle, Thrainsa & Wan for Ludum Dare 43.
All assets made during the event, except as listed below.

* Nobias Texture Pack (https://github.com/The-Yak/NobiaxTexturePack), CC0
(Some metallic textures)

* Game Audio Monthly #4, #5, #6 bundles (https://sonniss.com/gameaudiomonthly/), Sonniss Royalty-free license
(Some UI SFX, plus percussion elements for the music)

* Heavy Rain by Lebaston (https://freesound.org/people/lebaston100/sounds/243629/) CC-BY 3.0
(Rain loop, modified)